'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = job;

require('source-map-support/register');

var _kleioBodhi = require('kleio-bodhi');

var _kleioBodhi2 = _interopRequireDefault(_kleioBodhi);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
   TODO: Allow for customizable configurations from the outside through settings options.
   TODO: Add integration tests from kleio-bodhi to here
*/
function job(opts, done) {
  var _opts$settings = opts.settings;
  _opts$settings = _opts$settings === undefined ? {} : _opts$settings;
  var archiveFiles = _opts$settings.archiveFiles;
  var client = opts.connection;

  var kleioOpts = {
    client: client
  };

  return (0, _kleioBodhi2.default)(kleioOpts).archive(archiveFiles).finalize(done);
}
module.exports = exports['default'];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImluZGV4LmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7O2tCQU93QixHOztBQVB4Qjs7QUFDQTs7Ozs7Ozs7OztBQU1lLFNBQVMsR0FBVCxDQUFhLElBQWIsRUFBbUIsSUFBbkIsRUFBeUI7QUFBQSx1QkFDMEIsSUFEMUIsQ0FDOUIsUUFEOEI7QUFBQSxrREFDRCxFQURDO0FBQUEsTUFDbEIsWUFEa0Isa0JBQ2xCLFlBRGtCO0FBQUEsTUFDZSxNQURmLEdBQzBCLElBRDFCLENBQ0csVUFESDs7QUFFdEMsTUFBTSxZQUFZO0FBQ2hCO0FBRGdCLEdBQWxCOztBQUlBLFNBQU8sMEJBQU0sU0FBTixFQUNKLE9BREksQ0FDSSxZQURKLEVBRUosUUFGSSxDQUVLLElBRkwsQ0FBUDtBQUdEIiwiZmlsZSI6ImluZGV4LmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0ICdzb3VyY2UtbWFwLXN1cHBvcnQvcmVnaXN0ZXInO1xuaW1wb3J0IGtsZWlvIGZyb20gJ2tsZWlvLWJvZGhpJztcblxuLypcbiAgIFRPRE86IEFsbG93IGZvciBjdXN0b21pemFibGUgY29uZmlndXJhdGlvbnMgZnJvbSB0aGUgb3V0c2lkZSB0aHJvdWdoIHNldHRpbmdzIG9wdGlvbnMuXG4gICBUT0RPOiBBZGQgaW50ZWdyYXRpb24gdGVzdHMgZnJvbSBrbGVpby1ib2RoaSB0byBoZXJlXG4qL1xuZXhwb3J0IGRlZmF1bHQgZnVuY3Rpb24gam9iKG9wdHMsIGRvbmUpIHtcbiAgY29uc3QgeyBzZXR0aW5nczogeyBhcmNoaXZlRmlsZXMgfSA9IHt9LCBjb25uZWN0aW9uOiBjbGllbnQgfSA9IG9wdHM7XG4gIGNvbnN0IGtsZWlvT3B0cyA9IHtcbiAgICBjbGllbnRcbiAgfTtcblxuICByZXR1cm4ga2xlaW8oa2xlaW9PcHRzKVxuICAgIC5hcmNoaXZlKGFyY2hpdmVGaWxlcylcbiAgICAuZmluYWxpemUoZG9uZSk7XG59XG4iXX0=
//# sourceMappingURL=index.js.map
