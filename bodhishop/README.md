# README #

### What is this repository for? ###

This template provides much of the basic file/directory structure and setup required to quickly start coding a new job engine job with unit tests and test coverage reporting. It uses Mocha/Chai/Sinon for unit tests and Istanbul for test coverage reporting.

### How do I get set up? ###

1. Fork this repo and name your fork appropriately, e.g. yelp-job-getlocationdata.

2. Use Git to clone it to your development machine.

3. Run the **npm install** command to install dependencies into the node_modules directory.

4. Edit the package.json file as needed. At a minimum, edit these properties: [name, version, description, repository.url, author]

5. The index2.js file is included as a convenience. By adding an apiclient to the options.connection variable, you can run your job locally with the **node index2.js** command and use your apiclient credentials to send post/get requests to your namespace via the Bodhi API. This is most easily accomplished by adding a file called "apiclient.js" to your project and adding it to the .gitignore file so that you don't accidentally check in your credentials. You can then require the bodhi-driver-superagent module into that file and put your credentials there.

6. The test directory is where you put your unit tests. This directory structure should mimic the directory structure for your project. So, if all your functions are defined in lib/helper.js, for example, then you need a file called test/lib/helpers.spec.js for related unit tests.

7. The lib directory is where you should put all your job code.

8. Gruntfile: Run **grunt bumppush** to bump the version number and push code to your project repository on Bitbucket. 

7. Gruntfile: Run **grunt coverage** to run your unit tests.

8. After running your unit tests, open coverage/lcov-coverage/index.html with a browser to view test coverage statistics. You can click down into the directory structure to see a specific file. Istanbul will highlight the code that is not covered by you tests.

### Unit Test Examples ###

Unit test examples are provided to get you started. 