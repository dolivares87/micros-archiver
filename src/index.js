import 'source-map-support/register';
import kleio from 'kleio-bodhi';

/*
   TODO: Allow for customizable configurations from the outside through settings options.
   TODO: Add integration tests from kleio-bodhi to here
*/
export default function job(opts, done) {
  const { settings: { archiveFiles } = {}, connection: client } = opts;
  const kleioOpts = {
    client
  };

  return kleio(kleioOpts)
    .archive(archiveFiles)
    .finalize(done);
}
